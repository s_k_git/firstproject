import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';

const amountFormat = (amount) => {
  if (amount.length < 4) {
    return amount;
  }
  else {
    return amount.toString().replace(/,/g, "").trim().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
  }
}

const DateFormat = (date) => {
    const y = date.getFullYear();
    const m = ("00" + (date.getMonth()+1)).slice(-2);
    const d = ("00" + date.getDate()).slice(-2);
    const result = y + "-" + m + "-" + d;
    return result;
}

const IncomeForm = (props) => {
  const {classes} = props;
  const [incomeDate, setIncomeDate] = useState(() => DateFormat(new Date()));
  const [incomeContentId, setincomeContentId] = useState('0');
  const [incomeAmount, setIncomeAmount] = useState('');
  const [dateError, setDateError] = useState(null);
  const [amountError, setAmountError] = useState(null);

  const checkDate = (date) => {
    const reg = /^\d{4}-\d{1,2}-\d{1,2}$/;
    if (!reg.test(date)) {
      return "日付はyyyy/mm/dd形式で入力してください";
    }
    else {
      return null;
    }
  }

  const checkAmount = (amount) => {
    if (amount.trim() === "") {
      return "金額の入力は必須です";
    }
    else if (isNaN(amount.replace(/,/g, ""))) {
      return "金額は数値で入力してください";
    }
    else {
      return null;
    }
  }

  const checkInput = (e) => {
    e.preventDefault();
    console.log(incomeAmount);
    const dateErrorMessage = checkDate(incomeDate);
    const amountErrorMessage = checkAmount(incomeAmount);
    if (!dateErrorMessage && !amountErrorMessage) {
      props.save(incomeDate, props.contents[incomeContentId].name, amountFormat(Number(incomeAmount.replace(/,/g, ""))));
      setIncomeAmount("");
    }
    else {
      setDateError(dateErrorMessage);
      setAmountError(amountErrorMessage);
    }
  }

  return(
    <form onSubmit={(e) => checkInput(e)} >
      <Grid
        container
        className={classes.content}
        spacing={24}
        direction="column"
        justify="center"
        alignItems="center"
        alignContent="center"
      >
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="日付"
            type="date"
            InputLabelProps={{shrink: true}}
            onChange={(event) => {
              setIncomeDate(event.target.value);
              setDateError(null);
            }}
            value={incomeDate}
            error={dateError != null}
            helperText={dateError != null ? dateError : ""}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="収入内容"
            select
            onChange={(event) => setincomeContentId(event.target.value)}
            value={incomeContentId}
          >
            {
              props.contents.map((key, index, val) => (
              <MenuItem
                key={'income' + index}
                value={index}
              >
                {key.name}
              </MenuItem>
              ))
            }
          </TextField>
        </Grid>
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="収入金額"
            InputProps={{
              startAdornment: <InputAdornment position="start">&yen;</InputAdornment>,
            }}
            onChange={(event) => {
              setIncomeAmount(amountFormat(event.target.value));
              setAmountError(null);
            }}
            value={incomeAmount}
            error={amountError != null}
            helperText={amountError != null ? amountError : ""}
          />
        </Grid>
        <Grid item xs={6}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
          >
            登録
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default IncomeForm;
