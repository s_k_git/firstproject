import React, { useState, useEffect, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import ProgressWheel from '@material-ui/core/CircularProgress';
import IncomeForm from './IncomeForm.js';
import IncomeRepository from '../../repositories/IncomeRepository.js';
import IncomeContentRepository from '../../repositories/IncomeContentRepository.js';
import AuthManager from '../../auth/AuthManager.js';

import { Store } from '../../reducer.js';

const styles = (theme) => ({
  content: {
    minHeight: '100vh',
    maxWidth: '100vw'
  },
  inputForms: {
    width: '200px'
  },
  progressWheel: {
    marginLeft: '50%'
  }
});

const Income = (props) => {
  const {classes} = props;
  const [isLoading, setIsLoading] = useState(true);
  const [incomeContents, setIncomeContents] = useState([{}]);
  const { dispatch } = useContext(Store);

  useEffect(() => {
    dispatch({
      type: 'changeTitle',
      title: '収入入力'
    });
    setContents();
  }, [])

  const setContents = async () => {
    const contents = await IncomeContentRepository.get(AuthManager.getUserId());
    setIncomeContents(contents);
    setIsLoading(false);
  }

  const save = (date, content, amount) => {
    const splitDates = date.split('-');
    const insertDate = new Date(splitDates[0], splitDates[1] - 1, splitDates[2]);
    IncomeRepository.save(insertDate, content, amount, AuthManager.getUserId()).then(() => {
      alert('登録に成功しました');
    }).catch((error) => {
      alert('登録に失敗しました。');
    });
  }

  return(
    <div>
      {isLoading ?
        <Grid
          container
          className={classes.content}
          spacing={24}
          alignItems="center"
          justify="center"
        >
          <Grid item xs={10} md={6}>
            <ProgressWheel className={classes.progressWheel} />
          </Grid>
        </Grid> :
         <IncomeForm
            classes={classes}
            contents={incomeContents}
            save={(d, c, a) => save(d, c, a)}
         />
      }
    </div>
  )
}

export default withStyles(styles)(Income);
