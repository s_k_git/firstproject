import React, { useState, useContext } from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AuthManager from '../../auth/AuthManager.js';

import { Store } from '../../reducer.js';

const sideMenuItems = [
  {path : '/income', title : '収入入力'},
  {path : '/spending', title : '支出入力'},
  {path : '/list', title : '収支一覧'},
  {path : '/ContentManagement', title : '項目管理'}
]

const styles = (theme) => ({
  typographyStyle : {
    flexGrow : 1
  },
  iconStyle : {
    marginLeft : -12,
    marginRight : 20
  }
})

const Header = (props) => {
  const [showDrawer, setShowDrawer] = useState(false);
  const { state, dispatch } = useContext(Store);
  const {classes} = props;

  const logout = () => {
    AuthManager.logout().then(() => {
      dispatch({
        type : 'setCurrentUserId',
        id : ''
      });
      props.history.push('/');
    });
  }

  return(
    <div>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            className={classes.iconStyle}
            color="inherit"
            aria-label="Menu"
            onClick={() => setShowDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            className={classes.typographyStyle}
            variant="h6"
            color="inherit"
          >
            {state.title}
          </Typography>
          <Button color="inherit" onClick={logout}>
            Logout
          </Button>
        </Toolbar>
      </AppBar>
      <Drawer open={showDrawer} onClose={() => setShowDrawer(false)}>
          <List>
            {sideMenuItems.map((item, index) => (
              <ListItem
                button
                key={'sidemenu' + index}
                onClick={() => {
                  props.history.push(item.path);
                  setShowDrawer(false);
                }}
              >
                <ListItemText primary={item.title} />
              </ListItem>
            ))}
          </List>
      </Drawer>
    </div>
  );
}

export default withStyles(styles)(Header);
