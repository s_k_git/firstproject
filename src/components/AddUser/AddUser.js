import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import AuthManager from '../../auth/AuthManager.js';
import ProgressWheel from '@material-ui/core/CircularProgress';
import IncomeContentRepository from '../../repositories/IncomeContentRepository.js';
import CostContentRepository from '../../repositories/CostContentRepository.js';

const styles = (theme) => ({
  content: {
    minHeight: '100vh',
    maxWidth: '100vw'
  },
  progressWheel: {
    marginLeft: '50%'
  }
});

const AddUser = (props) => {
  const {classes} = props;
  const [mailAddress, setMailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [isErrorEmail, setIsErrorEmail] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState('');
  const [isErrorPassword, setIsErrorPassword] = useState(false);
  const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
  const [isLoading, setIsLoading] = useState(true);

  const addUser = (e) => {
    e.preventDefault();

    let isError = false;
    //入力値で判定できるエラーの対処
    if (mailAddress === null || mailAddress === "") {
      setError('emptyEmail');
      isError = true;
    }
    if (password === null || password === "") {
      setError('emptyPassword');
      isError = true;
    }
    else if (password.length < 6) {
      setError('auth/weak-password');
      isError = true;
    }

    if (!isError) {
      setIsLoading(true);
      AuthManager.addUser(mailAddress, password).then(async () => {
        await IncomeContentRepository.initialize(AuthManager.getUserId());
        await CostContentRepository.initialize(AuthManager.getUserId());
        alert('ユーザー登録に成功しました。');
        props.history.push('/');
      }).catch((error) => {
        setIsLoading(false);
        setError(error.code);
      });
    }
  }

  const setError = (errorCode) => {
    switch(errorCode) {
      case 'auth/invalid-email':
        setIsErrorEmail(true);
        setEmailErrorMessage('メールアドレスの形式が不正です');
        break;
      case 'auth/email-already-in-use':
        setIsErrorEmail(true);
        setEmailErrorMessage('メールアドレスは既に登録済みです');
        break;
      case 'auth/weak-password':
        setIsErrorPassword(true);
        setPasswordErrorMessage('パスワードは6文字以上必要です');
        break;
      case 'emptyEmail':
        setIsErrorEmail(true);
        setEmailErrorMessage('メールアドレスの入力は必須です');
        break;
      case 'emptyPassword':
        setIsErrorPassword(true);
        setPasswordErrorMessage('パスワードの入力は必須です');
        break;
      default:
        console.log(errorCode);
        break;
    }
  }

  useEffect(() => {
    setIsLoading(false);
  }, []);

  return(
    <span>
      {isLoading ?
        <Grid
          container
          className={classes.content}
          spacing={24}
          alignItems="center"
          justify="center"
        >
          <Grid item xs={10} md={6}>
             <ProgressWheel className={classes.progressWheel} />
          </Grid>
        </Grid>:
        <form onSubmit={(e) => addUser(e)}>
          <Grid
            container
            className={classes.content}
            spacing={16}
            direction="column"
            justify="center"
            alignItems="center"
            alignContent="center"
          >
            <Grid item xs={6} md={10}>
              <TextField
                placeholder="MailAddress"
                onChange={(event) => {
                  setMailAddress(event.target.value);
                  setIsErrorEmail(false);
                }}
                value={mailAddress}
                error={isErrorEmail}
                helperText={isErrorEmail ? emailErrorMessage : ''}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                type="password"
                placeholder="Password"
                onChange={(event) => {
                  setPassword(event.target.value);
                  setIsErrorPassword(false);
                }}
                value={password}
                error={isErrorPassword}
                helperText={isErrorPassword ? passwordErrorMessage : ''}
              />
            </Grid>
            <Grid item xs={4}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
              >
                登録
              </Button>
            </Grid>
            <Grid item xs={4}>
              <Button
                variant="contained"
                color="secondary"
                onClick={() => props.history.push('/')}
              >
                ログイン画面に戻る
              </Button>
            </Grid>
          </Grid>
        </form>
      }
    </span>
  );
}

export default withStyles(styles)(AddUser);
