import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const DataTable = (props) => {
  return(
    <Table>
      <TableHead>
        <TableRow>
          <CustomTableCell align="center">項目</CustomTableCell>
          <CustomTableCell align="center">金額(円)</CustomTableCell>
          <CustomTableCell align="center">日付</CustomTableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {props.list.map((item, index) => (
            <TableRow key={index}>
              <TableCell
                component="th"
                scope="row"
                style={{
                  paddingRight: '10px'
                }}
              >
                <Grid
                  container
                  spacing={40}
                  direction="row"
                  alignItems="center"
                >
                  <Grid item md={4} xs={4}>
                    <IconButton
                      style={{
                        marginLeft: '-20px',
                        marginRight: '20px'
                      }}
                      onClick={() => props.onDeleteClick(item.type, item.id)}
                    >
                      <ClearIcon />
                    </IconButton>
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={6}
                    style={{
                      paddingLeft: '5px',
                      paddingRight: '5px'
                    }}
                  >
                    {item.content}
                  </Grid>
                </Grid>
              </TableCell>
              <TableCell align="right">{item.amount}</TableCell>
              <TableCell
                align="right"
                style={{
                  width:"10%"
                }}
              >
                {props.dateFormat(item.date.toDate()).substring(5).replace('-', '/')}
              </TableCell>
            </TableRow>
        ))}
      </TableBody>
    </Table>
  )
};

export default DataTable;
