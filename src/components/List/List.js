import React, { useState, useEffect, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import ProgressWheel from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import BeforeButton from '@material-ui/icons/NavigateBefore';
import NextButton from '@material-ui/icons/NavigateNext';
import Button from '@material-ui/core/Button';
import DataTable from './DataTable.js';
import CostRepository from '../../repositories/CostRepository.js';
import IncomeRepository from '../../repositories/IncomeRepository.js';

import { Store } from '../../reducer.js';
import AuthManager from '../../auth/AuthManager.js';

const styles = (theme) => ({
  content: {
    minHeight: '100vh',
    maxWidth: '100vw'
  },
  progressWheel: {
    marginLeft: '50%'
  },
  heightControler: {
    height: '64px',
    [theme.breakpoints.down('xs')]: {
      paddingBottom: '40px'
    },
    [theme.breakpoints.up('sm')]: {
      paddingBottom: '60px'
    }
  }
});

const dateFormat = (date) => {
  const y = date.getFullYear();
  const m = ("00" + (date.getMonth()+1)).slice(-2);
  const d = ("00" + date.getDate()).slice(-2);
  const result = y + "-" + m + "-" + d;
  return result;
}

const monthFormat = (date) => {
  const y = date.getFullYear();
  const m = ("00" + (date.getMonth()+1)).slice(-2);
  const result = y + "-" + m;
  return result;
}

const List = (props) => {
  const {classes} = props;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);

  const { state, dispatch } = useContext(Store);

  const setDataTable = async (id, targetDate) => {
    const year = targetDate.getFullYear();
    const month = targetDate.getMonth();
    const start = new Date(year, month, 1);
    const end = new Date(year, month + 1, 1);

    const costs = await CostRepository.get(id, start, end);
    const incomes = await IncomeRepository.get(id, start, end);
    const mergedData = costs.concat(incomes);
    mergedData.sort((prev, next) => {
      if (prev.date.toDate() < next.date.toDate()) return -1;
      if (prev.date.toDate() > next.date.toDate()) return 1;
      return 0;
    });
    setData(mergedData);
    setIsLoading(false);
  }

  const deleteData = async(type, docId) => {
    const userId = AuthManager.getUserId()
    const deleted = (type === "income") ?
      IncomeRepository.delete(userId, docId) :
      CostRepository.delete(userId, docId);

    deleted.then(() => {
      console.log("Delete Success");
      setIsLoading(true);
      setDataTable(userId, state.targetDate);
    }).catch((e) => {
      console.log(e);
    });
  }

  const changeTargetDate = (targetDate, changeVal) => {
    setIsLoading(true);
    targetDate.setDate(1);
    targetDate.setMonth(targetDate.getMonth() + changeVal);
    dispatch({
      type: 'setTargetDate',
      date: targetDate
    });
    setDataTable(AuthManager.getUserId(), targetDate);
  }

  const resetTargetDate = () => {
    setIsLoading(true);
    const newTargetDate = new Date();
    dispatch({
      type: 'setTargetDate',
      date: newTargetDate
    });
    setDataTable(AuthManager.getUserId(), newTargetDate);
  }

  useEffect(() => {
    dispatch({
      type: 'changeTitle',
      title: '収支一覧'
    });

    setDataTable(AuthManager.getUserId(), state.targetDate);
  }, [])

  return(
    <span>
      {isLoading ?
        <Grid
          container
          className={classes.content}
          spacing={24}
          direction="column"
          justify="center"
          alignItems="center"
          alignContent="center"
        >
          <Grid item xs={10} md={6}>
            <ProgressWheel className={classes.progressWheel} />
          </Grid>
        </Grid> :
        <span>
          <div className={classes.heightControler}></div>
          <Grid
            container
            className={classes.content}
            spacing={24}
            direction="column"
            justify="top"
            alignItems="center"
          >
            <Grid item xs={10} md={6}>
              <Grid
                container
                spacing={40}
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Grid item xs={3} md={3}>
                  <IconButton
                    onClick={() => changeTargetDate(state.targetDate, -1)}
                  >
                    <BeforeButton/>
                  </IconButton>
                </Grid>
                <Grid item xs={6} md={6}>
                  <Typography variant="h4">
                    {state.targetDate.getFullYear() + "/" + ("00" + (state.targetDate.getMonth()+1)).slice(-2)}
                  </Typography>
                </Grid>
                <Grid item xs={3} md={3}>
                  <IconButton
                    onClick={() => changeTargetDate(state.targetDate, 1)}
                  >
                    <NextButton/>
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={10} md={10}>
              {data.length > 0 ?
                <DataTable
                   list={data}
                   onDeleteClick={(type, docId) => deleteData(type, docId)}
                   dateFormat={(date) => dateFormat(date)}
                /> :
                <p>データなし</p>
              }
            </Grid>
            <Grid item xs={10} md={6}>
              {monthFormat(state.targetDate) !== monthFormat(new Date()) &&
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => resetTargetDate()}
                >
                  今月へ戻る
                </Button>
              }
            </Grid>
          </Grid>
        </span>
      }
    </span>
  )
}

export default withStyles(styles)(List);
