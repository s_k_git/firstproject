import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import AuthManager from '../../auth/AuthManager.js';

const styles = (theme) => ({
  content: {
    minHeight: '100vh',
    maxWidth: '100vw'
  }
});

const Login = (props) => {
  const {classes} = props;
  const [loginMailAddress, setLoginMailAddress] = useState('');
  const [loginPassword, setLoginPassword] = useState('');
  const [isErrorEmail, setIsErrorEmail] = useState(false);
  const [isErrorPassword, setIsErrorPassword] = useState(false);

  const login = (e) => {
    e.preventDefault();
    const isEmptyEmail = loginMailAddress.trim() === "";
    const isEmptyPassword = loginPassword.trim() === "";

    if (!isEmptyEmail && !isEmptyPassword) {
      AuthManager.login(loginMailAddress, loginPassword).then((data) => {
        props.history.push('/income');
      }).catch((error) => {
        alert("ログインに失敗しました。\nメールアドレスまたはパスワードが誤っています。");
      });
    }
    else {
      setIsErrorEmail(isEmptyEmail);
      setIsErrorPassword(isEmptyPassword);
    }
  }

  return(
    <div>
      <form onSubmit={(e) => login(e)} >
        <Grid
          container
          className={classes.content}
          spacing={16}
          direction="column"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={6}>
            <TextField
              placeholder="MailAddress"
              onChange={(event) => {
                setLoginMailAddress(event.target.value);
                setIsErrorEmail(false);
              }}
              value={loginMailAddress}
              error={isErrorEmail}
              helperText={isErrorEmail ? 'メールアドレスの入力は必須です' : ''}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              type="password"
              placeholder="Password"
              onChange={(event) => {
                setLoginPassword(event.target.value);
                setIsErrorPassword(false);
              }}
              value={loginPassword}
              error={isErrorPassword}
              helperText={isErrorPassword ? 'パスワードの入力は必須です' : ''}
            />
          </Grid>
          <Grid item xs={6}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
            >
              ログイン
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => props.history.push('/addUser')}
            >
              ユーザー新規登録
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}

export default withStyles(styles)(Login);
