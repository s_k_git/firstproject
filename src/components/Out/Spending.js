import React, { useState, useEffect, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import ProgressWheel from '@material-ui/core/CircularProgress';
import SpendingForm from './SpendingForm.js';
import CostRepository from '../../repositories/CostRepository.js';
import CostContentRepository from '../../repositories/CostContentRepository.js';
import AuthManager from '../../auth/AuthManager.js';

import { Store } from '../../reducer.js';

const styles = (theme) => ({
  content: {
    minHeight: '100vh',
    maxWidth: '100vw'
  },
  inputForms: {
    width: '200px'
  },
  progressWheel: {
    marginLeft: '50%'
  }
});

const Spending = (props) => {
  const {classes} = props;
  const [isLoading, setIsLoading] = useState(true);
  const [spendingContents, setSpendingContents] = useState([{}]);
  const { dispatch } = useContext(Store);

  useEffect(() => {
    dispatch({
      type: 'changeTitle',
      title: '支出入力'
    });
    setContents();
  }, [])

  const setContents = async () => {
    const contents = await CostContentRepository.get(AuthManager.getUserId());
    setSpendingContents(contents);
    setIsLoading(false);
  }

  const save = (date, content, amount) => {
    const splitDates = date.split('-');
    const insertDate = new Date(splitDates[0], splitDates[1] - 1, splitDates[2]);
    CostRepository.save(insertDate, content, amount, AuthManager.getUserId()).then(() => {
      alert('登録に成功しました');
    }).catch((error) => {
      alert('登録に失敗しました。');
    });
  }

  return(
    <div>
      {isLoading ?
        <Grid
          container
          className={classes.content}
          spacing={24}
          alignItems="center"
          justify="center"
        >
          <Grid item xs={10} md={6}>
            <ProgressWheel className={classes.progressWheel} />
          </Grid>
        </Grid> :
         <SpendingForm
            classes={classes}
            contents={spendingContents}
            save={(d, c, a) => save(d, c, a)}
         />
      }
    </div>
  )
}

export default withStyles(styles)(Spending);
