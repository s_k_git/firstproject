import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';

const amountFormat = (amount) => {
  if (amount.length < 4) {
    return amount;
  }
  else {
    return amount.toString().replace(/,/g, "").trim().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
  }
}

const DateFormat = (date) => {
    const y = date.getFullYear();
    const m = ("00" + (date.getMonth()+1)).slice(-2);
    const d = ("00" + date.getDate()).slice(-2);
    const result = y + "-" + m + "-" + d;
    return result;
}

const SpendingForm = (props) => {
  const [spendingDate, setSpendingDate] = useState(DateFormat(new Date()));
  const [spendingContentId, setSpendingContentId] = useState('0');
  const [spendingAmount, setSpendingAmount] = useState('');
  const [dateError, setDateError] = useState(null);
  const [amountError, setAmountError] = useState(null);

  const checkDate = (date) => {
    const reg = /^\d{4}-\d{1,2}-\d{1,2}$/;
    if (!reg.test(date)) {
      return "日付はyyyy/mm/dd形式で入力してください";
    }
    else {
      return null;
    }
  }

  const checkAmount = (amount) => {
    if (amount.trim() === "") {
      return "金額の入力は必須です";
    }
    else if (isNaN(amount.replace(/,/g, ""))) {
      return "金額は数値で入力してください";
    }
    else {
      return null;
    }
  }

  const checkInput = (e) => {
    e.preventDefault();
    console.log(spendingAmount);
    const dateErrorMessage = checkDate(spendingDate);
    const amountErrorMessage = checkAmount(spendingAmount);
    if (!dateErrorMessage && !amountErrorMessage) {
      props.save(spendingDate, props.contents[spendingContentId].name, amountFormat(Number(spendingAmount.replace(/,/g, ""))));
      setSpendingAmount("");
    }
    else {
      setDateError(dateErrorMessage);
      setAmountError(amountErrorMessage);
    }
  }

  return(
    <form onSubmit={(e) => checkInput(e)}>
      <Grid
        container
        className={props.classes.content}
        spacing={24}
        direction="column"
        justify="center"
        alignItems="center"
        alignContent="center"
      >
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="日付"
            type="date"
            InputLabelProps={{shrink: true}}
            onChange={(event) => {
              setSpendingDate(event.target.value);
              setDateError(null);
            }}
            value={spendingDate}
            error={dateError != null}
            helperText={dateError != null ? dateError : ""}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="支出内容"
            select
            onChange={(event) => setSpendingContentId(event.target.value)}
            value={spendingContentId}
          >
            {props.contents.map((key, index, val) => (
              <MenuItem
                key={'spending' + index}
                value={index}
              >
                {key.name}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="支出金額"
            InputProps={{
              startAdornment: <InputAdornment position="start">&yen;</InputAdornment>,
            }}
            onChange={(event) => {
              setSpendingAmount(amountFormat(event.target.value));
              setAmountError(null);
            }}
            value={spendingAmount}
            error={amountError != null}
            helperText={amountError != null ? amountError : ''}
          />
        </Grid>
        <Grid item xs={6}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
          >
            登録
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default SpendingForm;
