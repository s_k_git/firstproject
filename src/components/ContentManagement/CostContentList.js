import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const CostContentList = (props) => {
  return(
    <Table>
      <TableHead>
        <TableRow>
          <CustomTableCell align="center">支出項目</CustomTableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {props.list.map((item, index) => (
            <TableRow key={index}>
              <TableCell
                component="th"
                scope="row"
                align="left"
              >
                {!item.isInitialContent &&
                  <IconButton
                    style={{
                      marginLeft: '-20px',
                      marginRight: '20px'
                    }}
                    onClick={() => props.onDeleteClick("cost", item.id)}
                  >
                    <ClearIcon />
                  </IconButton>
                }
                {item.name}
              </TableCell>
            </TableRow>
        ))}
      </TableBody>
    </Table>
  )
};

export default CostContentList;
