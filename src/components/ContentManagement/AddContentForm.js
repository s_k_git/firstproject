import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';

const types = ['収入', '支出'];

const AddContentForm = (props) => {
  const [typeId, setTypeId] = useState(0);
  const [contentName, setContentName] = useState("");
  const [contentNameError, setContentNameError] = useState(null);

  const checkInput = (e) => {
    e.preventDefault();
    const errorMessage = (contentName.trim() === "") ? "入力は必須です" : null;
    if (!errorMessage) {
      props.save(typeId, contentName);
    }
    else {
      setContentNameError(errorMessage);
    }
  }

  return(
    <form onSubmit={(e) => checkInput(e)} >
      <Grid
        container
        spacing={24}
        direction="column"
        justify="center"
        alignItems="center"
        alignContent="center"
      >
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="収支区分"
            select
            onChange={(e) => setTypeId(e.target.value)}
            value={typeId}
          >
            {
              types.map((val, index) => (
                <MenuItem
                  key={'type' + index}
                  value={index}
                >
                  {val}
                </MenuItem>
              ))
            }
          </TextField>
        </Grid>
        <Grid item xs={6}>
          <TextField
            className={props.classes.inputForms}
            label="項目内容"
            InputProps={{
              startAdornment: <InputAdornment position="start">{""}</InputAdornment>,
            }}
            onChange={(event) => {
              setContentName(event.target.value);
            }}
            value={contentName}
            error={contentNameError}
            helperText={!contentNameError ? "" : contentNameError}
          />
        </Grid>
        <Grid item xs={6}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
          >
            追加
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default AddContentForm;
