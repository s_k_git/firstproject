import React, { useState, useEffect, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';
import ProgressWheel from '@material-ui/core/CircularProgress';
import { Store } from '../../reducer.js';
import IncomeContentList from './IncomeContentList.js';
import CostContentList from './CostContentList.js';
import AddContentForm from './AddContentForm.js';
import CostContentRepository from '../../repositories/CostContentRepository.js';
import IncomeContentRepository from '../../repositories/IncomeContentRepository.js';
import AuthManager from '../../auth/AuthManager.js';

const styles = (theme) => ({
  content: {
    minHeight: '100vh',
    maxWidth: '100vw'
  },
  inputForms: {
    width: '200px'
  },
  progressWheel: {
    marginLeft: '50%'
  },
  heightController: {
    height: '64px',
    [theme.breakpoints.down('xs')]: {
      paddingBottom: '80px'
    },
    [theme.breakpoints.up('sm')]: {
      paddingBottom: '120px'
    }
  }
});

const ContentManagement = (props) => {
  const {classes} = props;
  const { dispatch } = useContext(Store);
  const [isLoading, setIsLoading] = useState(true);
  const [incomeContentList, setIncomeContentList] = useState([]);
  const [costContentList, setCostContentList] = useState([]);

  useEffect(() => {
    dispatch({
      type: 'changeTitle',
      title: '項目管理'
    });
    setContentList();
  }, []);

  const setContentList = async () => {
    const costContents = await CostContentRepository.get(AuthManager.getUserId());
    const incomeContents = await IncomeContentRepository.get(AuthManager.getUserId());
    incomeContents.sort((prev, next) => {
      if (prev.isInitialContent && !next.isInitialContent) return -1;
      if (!prev.isInitialContent && next.isInitialContent) return 1;
      return 0;
    });
    setIncomeContentList(incomeContents);
    costContents.sort((prev, next) => {
      if (prev.isInitialContent && !next.isInitialContent) return -1;
      if (!prev.isInitialContent && next.isInitialContent) return 1;
      return 0;
    });
    setCostContentList(costContents);
    setIsLoading(false);
  }

  const save = async (typeId, name) => {
    setIsLoading(true);
    const userId = AuthManager.getUserId();
    console.log(typeId);
    const saved = (typeId === 0) ?
      IncomeContentRepository.save(name, userId) :
      CostContentRepository.save(name, userId);
    saved.then(() => {
      setContentList();
    }).catch((e) => {
      console.log(e);
    });
  }

  const deleteData = async (type, docId) => {
    setIsLoading(true);
    const userId = AuthManager.getUserId();
    const deleted = (type === "income") ?
      IncomeContentRepository.delete(docId, userId) :
      CostContentRepository.delete(docId, userId);
    console.log(deleted);
    deleted.then(() => {
      setContentList();
    }).catch((e) => {
      setIsLoading(false);
      console.log(e);
    });
  }

  return(
    <span>
      {isLoading ?
        <Grid
          container
          className={classes.content}
          spacing={24}
          alignItems="center"
          justify="center"
        >
          <Grid item xs={10} md={6}>
             <ProgressWheel className={classes.progressWheel} />
          </Grid>
        </Grid> :
        <span>
          <div className={classes.heightController}></div>
          <Grid
            container
            className={classes.content}
            spacing={24}
            direction="column"
            justify="top"
          >
            <Grid item xs={12} md={12}>
              <Grid
                container
                spacing={24}
              >
                <Grid item xs={6} md={6}>
                   <IncomeContentList
                      list={incomeContentList}
                      onDeleteClick={(type, docId) => deleteData(type, docId)}
                   />
                </Grid>
                  <Grid item xs={6} md={6}>
                    <AddContentForm
                      classes={classes}
                      save={(type, name) => save(type, name)}
                    />
                  </Grid>
                <Grid item xs={6} md={6}>
                  <CostContentList
                    list={costContentList}
                    onDeleteClick={(type, docId) => deleteData(type, docId)}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </span>
      }
    </span>
  );
}

export default withStyles(styles)(ContentManagement);
