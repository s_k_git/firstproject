import firebase from '../firebase/firebaseInstance.js';

export default class CostContentRepository {
  static async initialize(userId) {
    const initialContents = [
      {name : '食費', isInitialContent : true},
      {name : '公共費', isInitialContent : true}
    ];

    const batch = firebase.firestore().batch();
    const colRef = firebase.firestore().collection('kakeibo').doc(userId).collection('costContent');

    initialContents.forEach((content) => {
      const docRef = colRef.doc();
      batch.set(docRef, content);
    });

    return await batch.commit();
  }

  static async save(name, userId) {
    return await
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('costContent')
      .add({
        name : name,
        isInitialContent : false
      });
  }

  static async get(userId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('costContent')
      .get()
      .then((snapshot) => {
        let tempArray = [];
        snapshot.forEach((doc) => {
          let data = doc.data();
          data["id"] = doc.id;
          tempArray.push(data);
        })
        return tempArray;
      })
    );
  }

  static async delete(docId, userId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('costContent')
      .doc(docId)
      .delete()
    );
  }
}
