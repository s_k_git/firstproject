import firebase from '../firebase/firebaseInstance.js';

export default class IncomeRepository {
  static async save(date, content, amount, userId) {
    return await (
      firebase.firestore().collection('kakeibo').doc(userId).collection('incomes').add({
        date : date,
        content : content,
        amount : amount
      })
    )
  }

  static async get(userId, startDate, endDate) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('incomes')
      .orderBy('date')
      .startAt(startDate)
      .endBefore(endDate)
      .get()
      .then((snapshot) => {
        let tempArray = [];
        snapshot.forEach((doc) => {
          let data = doc.data();
          data["id"] = doc.id;
          data["type"] = "income";
          tempArray.push(data);
        });
        return tempArray;
      })
    )
  }

  static async delete(userId, docId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('incomes')
      .doc(docId)
      .delete()
    )
  }
}
