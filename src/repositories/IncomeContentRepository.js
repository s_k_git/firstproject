import firebase from '../firebase/firebaseInstance.js';

export default class IncomeContentRepository {
  static async initialize(userId) {
    const initialContents = [
      {name : '基本収入', isInitialContent : true},
      {name : '臨時収入', isInitialContent : true}
    ];

    const batch = firebase.firestore().batch();
    const colRef = firebase.firestore().collection('kakeibo').doc(userId).collection('incomeContent');

    initialContents.forEach((content) => {
      const docRef = colRef.doc();
      batch.set(docRef, content);
    });

    return await batch.commit()
  }

  static async save(name, userId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('incomeContent')
      .add({
        name : name,
        isInitialContent : false
      })
    )
  }

  static async get(userId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('incomeContent')
      .get()
      .then((snapshot) => {
        let tempArray = [];
        console.log(snapshot);
        snapshot.forEach((doc) => {
          let data = doc.data();
          data["id"] = doc.id;
          tempArray.push(data);
        });
        return tempArray;
      })
    )
  }

  static async delete(docId, userId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('incomeContent')
      .doc(docId)
      .delete()
    )
  }
}
