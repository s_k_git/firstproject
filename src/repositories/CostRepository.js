import firebase from '../firebase/firebaseInstance.js';

export default class CostRepository {
  static async save(date, content, amount, userId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('costs')
      .add({
        date : date,
        content : content,
        amount : amount
      })
    )
  }

  static async get(userId, startDate, endDate) {
    return await(
       firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('costs')
      .orderBy('date')
      .startAt(startDate)
      .endBefore(endDate)
      .get()
      .then((snapshot) => {
        let tempArray = [];
        snapshot.forEach((doc) => {
          let data = doc.data();
          data["id"] = doc.id;
          data["type"] = "cost";
          tempArray.push(data);
        });
        return tempArray;
      })
    )
  }

  static async delete(userId, docId) {
    return await(
      firebase
      .firestore()
      .collection('kakeibo')
      .doc(userId)
      .collection('costs')
      .doc(docId)
      .delete()
    )
  }
}
