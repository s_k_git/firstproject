import React, { useReducer } from 'react';

const initialState = {
  title : '',
  targetDate : new Date()
}

const Store = React.createContext();

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case 'changeTitle':
      return{
        ...state,
        title : action.title
      }
    case 'setTargetDate':
      return{
        ...state,
        targetDate : action.date
      }
    default:
      return state;
  }
}

const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return(
    <Store.Provider value={{state, dispatch}}>
      {children}
    </Store.Provider>
  )
}

export { Store, Provider };
