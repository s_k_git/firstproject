import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';
import Login from './components/Login/Login.js';
import AddUser from './components/AddUser/AddUser.js';
import Header from './components/Header/Header.js';
import Income from './components/Income/Income.js';
import Spending from './components/Out/Spending.js';
import List from './components/List/List.js';
import ContentManagement from './components/ContentManagement/ContentManagement.js';
import { Provider } from './reducer.js';

const Main = (props) => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/addUser" component={AddUser} />
          <Route component={Header} />
        </Switch>
        <Route path="/income" component={Income} />
        <Route path="/spending" component={Spending} />
        <Route path="/list" component={List} />
        <Route path="/ContentManagement" component={ContentManagement} />
      </div>
    </BrowserRouter>
  )
}

const App = (props) => {
  return (
    <Provider>
      <Main />
    </Provider>
  )
}

export default App;
