import firebase from '../firebase/firebaseInstance.js';

const SESSION_KEY = `${document.domain}-user-info`;

export default class AuthManager {
  static async addUser(mail, password) {
    return await firebase.auth().createUserWithEmailAndPassword(mail, password).then(() => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          localStorage[SESSION_KEY] = user.uid;
        }
      });
    });
  }

  static async login(mail, password) {
    return await firebase.auth().signInWithEmailAndPassword(mail, password).then((data) => {
      localStorage[SESSION_KEY] = data.user.uid;
    });
  }

  static async logout() {
    return await firebase.auth().signOut().then(() => {
      localStorage[SESSION_KEY] = null;
    });
  }

  static getUserId() {
    return localStorage[SESSION_KEY];
  }
}
